FROM tomcat:8.0
LABEL maintainer="Miguel Vila Huallpa"

LABEL artifact_id="spring-petclinic"
LABEL version="2.1.3"

COPY petclinic.war /usr/local/tomcat/webapps

ENV USER=petclinic
ENV UID=10014
ENV GID=10014
RUN addgroup "$USER" --gid "$GID" \
    && adduser \
    --disabled-password \
    --gecos "" \
    --home "$(pwd)" \
    --ingroup "$USER" \
    --no-create-home \
    --uid "$UID" \
    "$USER"
RUN chown -R "$USER":"$USER" /usr/local/tomcat/webapps

USER "$USER"